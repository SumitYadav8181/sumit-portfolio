module.exports = {
  theme: {
    fontSize: {
      'xs': '.75rem',
      'sm': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
      '8xl': '7rem',
    },
    // textColor: {
    //   'primary': '#1801fd',
    //   'secondary': '#ffed4a',
    //   'danger': '#e3342f',
    // },


    lineHeight: {
      none: 1,
      tight: 1.25,
      snug: 1.375,
      normal: 1.5,
      relaxed: 1.625,
      relaxed: 1.75,
      loose: 2,
      title: 0.8,
    }
  },
  variants: {},
  plugins: [],
}
